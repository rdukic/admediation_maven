# Ad mediation #

System checks for URL parameters which should be information about the device that you're using. In the latest version, it can use _countryCode_ and _osVersion_ for filtering ads. 

## Requirements 

* Eclipse,
* Google Cloud Platform (GCP) plugin for Eclipse, [link](https://cloud.google.com/eclipse/docs/quickstart)
* Maven

## Installation ##

After downloading the project, navigate with terminal to it and use:

            mvn clean install

## Running ##

In Eclipse, open the project, navigate to the **Servers**, right click on the server and click on Run. Then the site will be available at localhost:8080

## Deployment ##

In Eclipse, open the project, navigate to the **Project Explorer**, right click on the project and click on **Deploy to App Engine Standard**. After that follow the instructions.

## Usage ##

 * http://localhost:8080/fetchAdProviders - returns the full list of ad providers,
 * http://localhost:8080/fetchAdProviders?osVersion=9 - returns a filtered list of ad providers. The filtering is based on the OS version of the device.
 * http://localhost:8080/fetchAdProviders?countryCode=CN - returns a filtered list of ad providers. The filtering is based on the geographic location. In the specific example it's filtering all ad providers that shouldn't be used in China.
 * http://localhost:8080/fetchAdProviders?countryCode=CN&osVersion=9 - returns a filtered list of ad providers. The filtering takes into account the OS version as well as geographic location of the device.

## Description ##

### Storage ###

Currently each AdFilter object consists of:
 * String that contains JSON of up to date ordered list of available ad providers. The JSON contains separate lists for each ad type. In the case that ad categories would be added in the future, the filter shouldn't have an issue as it checks each category separately,
 * String array that contains all available ad providers. The array is used for adding opted out ad providers.
 * String that contains a JSON of "banned" ad providers. The JSON contains separate separate lists for each criteria. 

Ad provider information is stored as an AdProvider object which currently contain the name of the ad provider (e.g. "Unity Ads") and it's URL to use (this is how I assume ad providers are used so it might not be completely right)

### Step by step example ###

1. When the get request is received:
    1. The parameter map is fetched and stored,
    2. AdFilter is instantiated (at this point all the necessary information for successful filtering is fetched)
2. fetchAdProviders is called
    1. fetchAdProviders relays client properties to filterAds,
3. filterAds is called
    1. Ad provider list is filtered for each category separately
        * Filtering is done for each client property.
        * Filter checks the ad provider list with the "banned" ad provider list.
    2. Ad provider list is checked and in case any of the ad providers are missing, the opted out version of the ad provider is created and added to the end of the list,
    3. If there's a problem and filtering cannot be succesfully done the unfiltered list is returned
4. Filtered ad provider list is returned as a JSON.


## Problems / Thoughts ##

Currently the way the system works is that every time a request is sent, the AdFilter object is initiated which means that the data it's working with cannot be updated. This problem could be solved by using a database (preferably noSQL as the data is currently stored in a JSON format). I would resolve this by using Firebase for persistant storage and a fallback source of up to date information about ad providers which would be able to be updated via a post request with the updated JSON. The constructor of the AdFilter object has a variant that requires the string with the updated JSON that would then be replaced in the Firebase database.

I also planned to implement the use of GCP's Cache for improved performance of data fetching. While the CacheProvider class is present in the repository, it has been abandoned in an earlier version because of the Cache's lack of persistent data which would result in a sudden loss of the ad provider list. Because of that I'd have to implement the connection to Firebase first but I estimated that I wouldn't be able to finish the project before deadline.

Apart from the up to date ad provider list I'd also include a JSON with "banned" ad providers in a format that's shown in **bannedAdProviders.json** file and a list of all available ad providers in case there would be a need for filtering any incorrectly input information and/or adding all missing ad providers that were opted out. This might be unnecessary if ordered list already filtered out any incorrectly input information.

Filtering might be able to be optimized since the current version filters the list for each criteria separately. If it is possible to create a list of "banned" ad providers by combining all the lists of submitted client properties, it could be faster because there would be only one sweep of the ad provider list.

During various iterations the _fetchAdProviders_ function shrunk to just one call which resulted in it becoming unnecessary. It might be a better way to just make _filterAds_ public and remove _fetchAdProviders_.

### Tests ###

I didn't set up any specific unit tests as I didn't have enough time to learn how to use the Surefire plugin. I did use [Postman](https://www.postman.com/) to generate various queries to test the functionality. Additionaly the JSON file that's used as an example, contained data in the way so I could test the example, given by the test description.

### Source control ###

 * master - contains the last stable version,
 * development - for making minor fixes. Additional functionality and/or bigger fixes will be done in a separate branch and then merged to this one.

### Author ###

* Rok Dukič - rok1071@gmail.com