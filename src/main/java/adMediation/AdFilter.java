package adMediation;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;



public class AdFilter{

	private String adProvidersJSON;
	
	private String[] allAdProviders = {"Adx", "AdMob", "Unity Ads", "Facebook"};

	private String bannedAdProvidersJSON;	

	
	
public AdFilter() {
	
		Map<String, HashMap<String, ArrayList<String>>> bannedAdProviders = new HashMap<String, HashMap<String, ArrayList<String>>>();;
		
		
		HashMap<String,ArrayList<String>> bannedAdProvidersPerCountry = new HashMap<String,ArrayList<String>>();
		HashMap<String,ArrayList<String>> bannedAdProvidersPerOSVer = new HashMap<String,ArrayList<String>>();
		
		bannedAdProvidersPerCountry.put("CN", new ArrayList<String>() {{
			add("Facebook");
		}});
		
		bannedAdProvidersPerOSVer.put("9", new ArrayList<String>() {{
			add("AdMob");
		}});

		
		bannedAdProviders.put("countryCode", bannedAdProvidersPerCountry);
		bannedAdProviders.put("osVersion", bannedAdProvidersPerOSVer);
		
		this.bannedAdProvidersJSON = new Gson().toJson(bannedAdProviders, new TypeToken<Map<String, HashMap<String, ArrayList<String>>>>(){}.getType());
					
		this.adProvidersJSON = "{\"Banner\":[{\"name\":\"Adx\",\"URL\":\"\"},{\"name\":\"AdMob\",\"URL\":\"\"},{\"name\":\"Unity Ads\",\"URL\":\"\"},{\"name\":\"Facebook\",\"URL\":\"\"}],\"Interstitial\":[{\"name\":\"Adx\",\"URL\":\"\"},{\"name\":\"Unity Ads\",\"URL\":\"\"},{\"name\":\"Facebook\",\"URL\":\"\"}],\"RewardedVideo\":[{\"name\":\"Adx\",\"URL\":\"\"},{\"name\":\"Facebook\",\"URL\":\"\"},{\"name\":\"AdMob\",\"URL\":\"\"},{\"name\":\"Unity Ads\",\"URL\":\"\"}]}";								

	}
	
	public AdFilter(String jsonAdProviders ) {
	
		if(this.bannedAdProvidersJSON.isEmpty()) {
			Map<String, HashMap<String, ArrayList<String>>> bannedAdProviders = new HashMap<String, HashMap<String, ArrayList<String>>>();;
						
			HashMap<String,ArrayList<String>> bannedAdProvidersPerCountry = new HashMap<String,ArrayList<String>>();
			HashMap<String,ArrayList<String>> bannedAdProvidersPerOSVer = new HashMap<String,ArrayList<String>>();
			
			bannedAdProvidersPerCountry.put("CN", new ArrayList<String>() {{
				add("Facebook");
			}});
			
			bannedAdProvidersPerOSVer.put("9", new ArrayList<String>() {{
				add("AdMob");
			}});
			
			bannedAdProviders.put("countryCode", bannedAdProvidersPerCountry);
			bannedAdProviders.put("osVersion", bannedAdProvidersPerOSVer);
			
			this.bannedAdProvidersJSON = new Gson().toJson(bannedAdProviders, new TypeToken<Map<String, HashMap<String, ArrayList<String>>>>(){}.getType());
			
		}
		
		this.adProvidersJSON = jsonAdProviders;
		
	}
	
	
	
	private String filterAds(Map<String,String[]> clientProperties){
		try {
			JsonObject adProviders = new JsonParser().parse(this.adProvidersJSON).getAsJsonObject();
										
			Set<String> adCategories = adProviders.keySet();
			
			adCategories.forEach((category) -> {
				
				JsonArray adProviderCategoryArr = adProviders.get(category).getAsJsonArray();
				
				if(clientProperties.get("countryCode") != null) {								
					filter("countryCode", clientProperties.get("countryCode")[0], adProviderCategoryArr);
				}
				if(clientProperties.get("osVersion") != null) {
					filter("osVersion", clientProperties.get("osVersion")[0], adProviderCategoryArr);
				}

			
				for(String adProvider: allAdProviders) {
					String currentAdProvider = new Gson().toJson(adProviders.get(category));					
					
					if(currentAdProvider.contains(adProvider)) {
						for(int i = 0; i < adProviderCategoryArr.size(); i++) {
							if(adProviderCategoryArr.get(i).getAsJsonObject().get("name").getAsString().equals(adProvider + "-OptOut")) {
								adProviderCategoryArr.remove(i);
								break;
							}
						}
					}else {
						JsonObject optOut = new JsonObject();
						optOut.addProperty("name", adProvider + "-OptOut");
						optOut.addProperty("URL", "");
						adProviderCategoryArr.add(optOut);
					}
				}
			});
	
			String result = new Gson().toJson(adProviders);
			
			return result;
		
		}catch(Exception e) {
			System.out.print(e.getMessage().toString());
		}
		
		return this.adProvidersJSON;
	}
	
	public String fetchAdProviders(Map<String,String[]> clientProperties) {
		return filterAds(clientProperties);			
	}
	

	public void filter(String criteria, String clientInfo, JsonArray adProviders) {
		
		JsonArray bannedAdProviders = new JsonParser().parse(bannedAdProvidersJSON).getAsJsonObject().get(criteria).getAsJsonObject().get(clientInfo).getAsJsonArray();	
		
		for(int x = 0; x < bannedAdProviders.size(); x++) {
			for(int i = 0; i < adProviders.size(); i++) {
				if(adProviders.get(i).getAsJsonObject().get("name").getAsString().equals(bannedAdProviders.get(x).getAsString())) {
					adProviders.remove(i);
					break;
				}
			}
		}

	}
	

}