package adMediation;

import java.io.IOException;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "HelloAppEngine",
    urlPatterns = {"/fetchAdProviders"}
)
public class AdMediationEngine extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
	  
		Map<String, String[]> parMap = request.getParameterMap();
		
		AdFilter aF = new AdFilter();
		
		String a = aF.fetchAdProviders(parMap);
		
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");

	    response.getWriter().print(a.toString());
	    

  }
  
  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)
  	throws IOException {
	    
	  //read request's body for JSON.
	  
	  String updatedAdProviderList = "{\"Banner\":[{\"name\":\"Adx\",\"URL\":\"\"},{\"name\":\"AdMob\",\"URL\":\"\"},{\"name\":\"Unity Ads\",\"URL\":\"\"},{\"name\":\"Facebook\",\"URL\":\"\"}],\"Interstitial\":[{\"name\":\"Adx\",\"URL\":\"\"},{\"name\":\"Unity Ads\",\"URL\":\"\"},{\"name\":\"Facebook\",\"URL\":\"\"}],\"RewardedVideo\":[{\"name\":\"Adx\",\"URL\":\"\"},{\"name\":\"Facebook\",\"URL\":\"\"},{\"name\":\"AdMob\",\"URL\":\"\"},{\"name\":\"Unity Ads\",\"URL\":\"\"}]}";
	  
	  //connect to database and replace the ad provider list.
  }

}