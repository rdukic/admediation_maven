package adMediation;


import java.util.ArrayList;
import java.util.HashMap;

import javax.cache.Cache;
import javax.cache.CacheException;
import javax.cache.CacheFactory;
import javax.cache.CacheManager;

public class CacheHandler {

	private Cache adProvidersCache;
	private Cache bannedAdProvidersCache;
	private CacheFactory cacheFactory;
	
	
	public CacheHandler() {
		try {
				
			
			HashMap<String, String> testMap = new HashMap<String,String>() {{
				put("Adx", "");
				put("AdMob", "");
				put("Unity Ads", "");
				put("Facebook", "");
			}};
			
			HashMap<String, String> testMapNoAdMob = new HashMap<String,String>() {{
				put("Adx", "");
				put("Unity Ads", "");
				put("Facebook", "");
			}};
			
			this.cacheFactory = CacheManager.getInstance().getCacheFactory();
			if(this.adProvidersCache.isEmpty()) {
				this.adProvidersCache = cacheFactory.createCache(new HashMap<String, HashMap<String,String>>());
				this.adProvidersCache.put("Banner", testMap);
				this.adProvidersCache.put("Interstitial", testMapNoAdMob);
				this.adProvidersCache.put("RewardedVideo", testMap);
			}
					
			if(this.bannedAdProvidersCache.isEmpty()) {
				this.bannedAdProvidersCache = cacheFactory.createCache(new HashMap<String, HashMap<String,ArrayList<String>>>());
				HashMap<String, ArrayList<String>> bannedAdProvidersPerCountry = new HashMap<String,ArrayList<String>>();
				bannedAdProvidersPerCountry.put("CN", new ArrayList<String>() {{
					add("Facebook");
				}});
				
				HashMap<String, ArrayList<String>> bannedAdProvidersPerOSVersion = new HashMap<String,ArrayList<String>>();
				bannedAdProvidersPerOSVersion.put("9", new ArrayList<String>() {{
					add("AdMob");
				}});				
				
				this.bannedAdProvidersCache.put("countryCode", bannedAdProvidersPerCountry );
				this.bannedAdProvidersCache.put("osVersion", bannedAdProvidersPerOSVersion );
			}		
			
		}catch(CacheException ce) {
			//handle exception
		}
		
	}
	
	public HashMap<String, HashMap<String,String>> fetchAdProviders(){
		HashMap<String, HashMap<String,String>> adProviderMap = new HashMap<String, HashMap<String,String>>();
		adProviderMap.put("Banner", (HashMap<String, String>) this.adProvidersCache.get("Banner"));
		adProviderMap.put("Interstitial", (HashMap<String, String>) this.adProvidersCache.get("InterStitial"));
		adProviderMap.put("RewardedVideo", (HashMap<String, String>) this.adProvidersCache.get("RewardedVideo"));		
		return adProviderMap;
	}
	
	public HashMap<String, String[]> fetchBannedAdProviders(String criteria){
		HashMap<String, String[]> bannedAdProviders = new HashMap<String, String[]>();
		bannedAdProviders = (HashMap<String, String[]>) this.bannedAdProvidersCache.get(criteria);
		return bannedAdProviders;
	}
	
	
	
}
